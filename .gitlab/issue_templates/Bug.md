<!---
Please read this!

Before opening a new issue, make sure to search for keywords in the issues
filtered by the "bug" label:

- https://gitlab.com/gitlab-org/gitlab-jetbrains-plugin/-/issues?label_name%5B%5D=type::bug

and verify the issue you're about to submit isn't a duplicate.
--->

### Checklist

### Summary

<!-- Summarize the bug encountered concisely -->

### Steps to reproduce

<!-- How one can reproduce the issue - this is very important -->

### What is the current _bug_ behavior?

<!-- What actually happens -->

### What is the expected _correct_ behavior?

<!-- What you should see instead -->

### Relevant logs and/or screenshots

### Possible fixes

<!-- If you can, link to the line of code that might be responsible for the problem -->

/label ~"type::bug" ~"devops::create" ~"group::code review" ~"Category:Editor Extension" ~"section::dev" ~jetbrains
