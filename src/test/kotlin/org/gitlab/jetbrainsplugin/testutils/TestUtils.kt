package org.gitlab.jetbrainsplugin.testutils

import com.github.tomakehurst.wiremock.client.WireMock
import com.github.tomakehurst.wiremock.junit.WireMockRule

fun WireMockRule.stubGetJsonResponse(requestUrl: String, jsonResponse: String) {
    this.stubFor(
        WireMock.get(WireMock.urlEqualTo(requestUrl))
            .willReturn(
                WireMock.aResponse()
                    .withHeader("Content-Type", "application/json")
                    .withBody(jsonResponse)
            )
    )
}
