package org.gitlab.jetbrainsplugin.testutils

import git4idea.repo.GitRemote
import git4idea.repo.GitRepository
import org.gitlab.jetbrainsplugin.git.GitRemoteUrlCoordinates
import org.gitlab.jetbrainsplugin.git.GlGitRepositoryMapping
import org.gitlab.jetbrainsplugin.gitlab.GlRepositoryCoordinates
import org.gitlab.jetbrainsplugin.gitlab.GlRepositoryPath
import org.gitlab.jetbrainsplugin.settings.account.GlAccount
import org.gitlab.jetbrainsplugin.settings.account.GlServerPath
import org.mockito.Mockito

object TestData {
    const val port = 8089
    val serverPath = GlServerPath("http://localhost:$port")
    val account = GlAccount("test", serverPath)

    object Git {
        const val remoteUrl = "git@localhost:$port/group/project"
        val gitRemote = GitRemote("origin", listOf(remoteUrl), listOf(remoteUrl), listOf(), listOf())
        val repositoryCoordinates = GlRepositoryCoordinates(
            serverPath,
            GlRepositoryPath("group", "project")
        )
        val remoteUrlCoordinates = GitRemoteUrlCoordinates(
            remoteUrl,
            gitRemote,
            Mockito.mock(GitRepository::class.java)
        )
        val repositoryMapping = GlGitRepositoryMapping(repositoryCoordinates, remoteUrlCoordinates)
    }

    object Responses {
        const val projectResponse = """{ "id": 1, "name": "project-name" }"""
        const val userResponse = """{ "name": "Josh", "username": "josh" }"""
    }
}
