package org.gitlab.jetbrainsplugin.validation

import org.gitlab.jetbrainsplugin.GitLabBundle.message
import org.junit.Test
import kotlin.test.assertContains
import kotlin.test.assertNull

class UrlValidatorTest {

    @Test
    fun `valid URL is valid`() {
        assertNull(UrlValidator.validateUrl("https://gitlab.com"))
    }

    @Test
    fun `URL must be with http or https protocol`() {
        assertContains(UrlValidator.validateUrl("ftp://gitlab.com")!!, message("validation.instanceUrl.https"))
        assertContains(UrlValidator.validateUrl("gitlab.com")!!, message("validation.instanceUrl.https"))
    }

    @Test
    fun `URL is invalid if it can't be parsed`() {
        assertContains(
            UrlValidator.validateUrl("https://gitlab.com/@#$%^&*(")!!,
            "URL is not valid"
        )
    }
}
