package org.gitlab.jetbrainsplugin.settings.account

import com.intellij.configurationStore.deserializeAndLoadState
import com.intellij.configurationStore.serialize
import com.intellij.openapi.util.JDOMUtil
import org.junit.Assert
import org.junit.Test
import kotlin.test.assertEquals

class GlAccountsRepositoryTest {
    private val uuid = "09eb7269-03b3-4bd6-8fd5-6ed3fe2bdb2d"

    @Test
    fun `test serialize`() {
        val service = GlAccountsRepository()
        val path = GlServerPath()
        path.instanceUrl = "https://gitlab.com"
        val account = GlAccount("ignored name", path, uuid)
        service.accounts = setOf(account)
        @Suppress("UsePropertyAccessSyntax") val state = service.getState()
        val element = serialize(state)!!
        val xml = JDOMUtil.write(element)
        Assert.assertEquals(
            """
            <array>
              <account id="$uuid">
                <server instanceUrl="https://gitlab.com" />
              </account>
            </array>
            """.trimIndent(),
            xml
        )
    }

    @Test
    fun `test deserialize`() {
        val service = GlAccountsRepository()

        val element = JDOMUtil.load(
            """
            <array>
              <account name="account name" id="$uuid">
                <server instanceUrl="https://gitlab.com" />
              </account>
            </array>
            """.trimIndent()
        )
        deserializeAndLoadState(service, element)
        assertEquals(service.accounts.size, 1)
        val result = service.accounts.first()
        assertEquals(result.name, "")
        assertEquals(result.id, uuid)
        assertEquals(result.server.instanceUrl, "https://gitlab.com")
    }
}
