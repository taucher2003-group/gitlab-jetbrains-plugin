package org.gitlab.jetbrainsplugin.settings.account

import org.junit.Test
import kotlin.test.assertEquals

class GlServerPathTest {
    @Test
    fun `toString removes protocol`() {
        assertEquals("gitlab.com", GlServerPath("https://gitlab.com").toString())
        assertEquals("example.gitlab.com/gitlab", GlServerPath("https://example.gitlab.com/gitlab").toString())
    }
}
