package org.gitlab.jetbrainsplugin.settings.authentication

import org.gitlab.jetbrainsplugin.settings.account.GlServerPath
import org.junit.Test
import kotlin.test.assertEquals

class GlSecurityUtilTest {
    @Test
    fun `buildNewTokenUrl generates URL with IDE name and token scopes`() {
        val result = GlSecurityUtil.buildNewTokenUrl(GlServerPath.DEFAULT_SERVER)
        assertEquals(
            "https://gitlab.com/-/profile/personal_access_tokens?name=IntelliJ%20IDEA%20GitLab%20Integration%20Plugin&scopes=api%2Cread_user",
            result
        )
    }
}
