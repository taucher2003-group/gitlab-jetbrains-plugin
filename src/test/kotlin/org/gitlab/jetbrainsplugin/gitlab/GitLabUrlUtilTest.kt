/*
Copyright 2000-2021 JetBrains s.r.o.
Licensed under the Apache License, Version 2.0 (the "License").
You may not use this file except in compliance with the License.
You may obtain a copy of the License at www.apache.org/licenses/LICENSE-2.0.txt.
*/
package org.gitlab.jetbrainsplugin.gitlab

import com.intellij.openapi.util.Pair
import junit.framework.TestCase
import org.gitlab.jetbrainsplugin.gitlab.GitLabUrlUtil.getNamespaceAndProjectFromRemoteUrl

class GitLabUrlUtilTest : TestCase() {
    private class TestCase<T> {
        val tests: MutableList<Pair<String, T>> = ArrayList()

        fun add(`in`: String, out: T?) {
            tests.add(Pair.create(`in`, out))
        }
    }

    private fun <T> runTestCase(tests: TestCase<T>, func: (String) -> T) {
        for (test in tests.tests) {
            val input = test.getFirst()
            val expectedResult = test.getSecond()
            val result = func(input)
            assertEquals(input, expectedResult, result)
        }
    }

    fun testGetUserAndRepositoryFromRemoteUrl() {
        val tests = TestCase<GlRepositoryPath?>()

        tests.add("http://gitlab.com/username/reponame/", GlRepositoryPath("username", "reponame"))
        tests.add("https://gitlab.com/username/reponame/", GlRepositoryPath("username", "reponame"))
        tests.add("git://gitlab.com/username/reponame/", GlRepositoryPath("username", "reponame"))
        tests.add("git@gitlab.com:username/reponame/", GlRepositoryPath("username", "reponame"))

        tests.add("https://gitlab.com/username/reponame", GlRepositoryPath("username", "reponame"))
        tests.add("https://gitlab.com/username/reponame.git", GlRepositoryPath("username", "reponame"))
        tests.add("https://gitlab.com/username/reponame.git/", GlRepositoryPath("username", "reponame"))
        tests.add("git@gitlab.com:username/reponame.git/", GlRepositoryPath("username", "reponame"))
        tests.add("git@gitlab.com:username/reponame.git", GlRepositoryPath("username", "reponame"))
        tests.add(
            "git@gitlab.com:gitlab-org/security/gitlab-vscode-extension.git",
            GlRepositoryPath("gitlab-org/security", "gitlab-vscode-extension")
        )
        tests.add(
            "https://gitlab.com/gitlab-org/security/gitlab-vscode-extension.git",
            GlRepositoryPath("gitlab-org/security", "gitlab-vscode-extension")
        )
        tests.add(
            "git@gitlab.com:gitlab-org/security-products/analyzers/fuzzers/jsfuzz.git",
            GlRepositoryPath("gitlab-org/security-products/analyzers/fuzzers", "jsfuzz")
        )

        tests.add(
            "http://login:passsword@gitlab.com/username/reponame/",
            GlRepositoryPath("username", "reponame")
        )

        tests.add("HTTPS://gitlab.com/username/reponame/", GlRepositoryPath("username", "reponame"))
        tests.add("https://gitlab.com/UserName/RepoName/", GlRepositoryPath("UserName", "RepoName"))

        tests.add("https://gitlab.com/RepoName/", null)
        tests.add("git@gitlab.com:user/", null)
        tests.add("https://user:pass@gitlab.com/", null)

        runTestCase(tests) { `in` -> getNamespaceAndProjectFromRemoteUrl(`in`) }
    }
}
