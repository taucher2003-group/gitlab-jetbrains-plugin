package org.gitlab.jetbrainsplugin.gitlab

import com.github.tomakehurst.wiremock.junit.WireMockRule
import com.intellij.testFramework.fixtures.BasePlatformTestCase
import com.intellij.testFramework.replaceService
import com.intellij.util.application
import org.gitlab.jetbrainsplugin.git.GlProjectRepositoriesManager
import org.gitlab.jetbrainsplugin.settings.account.GlAccountManager
import org.gitlab.jetbrainsplugin.testutils.TestData
import org.gitlab.jetbrainsplugin.testutils.TestData.Git.repositoryMapping
import org.gitlab.jetbrainsplugin.testutils.TestData.Responses.projectResponse
import org.gitlab.jetbrainsplugin.testutils.TestData.account
import org.gitlab.jetbrainsplugin.testutils.stubGetJsonResponse
import org.gitlab.jetbrainsplugin.utils.getOrHandle
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito

@RunWith(JUnit4::class)
class GlProjectServiceTest : BasePlatformTestCase() {

    @get:Rule
    var server = WireMockRule(TestData.port)

    override fun setUp() {
        super.setUp()
        val mockedRepositoriesManager = Mockito.mock(GlProjectRepositoriesManager::class.java)
        val mockedAccountManager = Mockito.mock(GlAccountManager::class.java)
        project.replaceService(GlProjectRepositoriesManager::class.java, mockedRepositoriesManager, testRootDisposable)
        application.replaceService(GlAccountManager::class.java, mockedAccountManager, testRootDisposable)
        Mockito.`when`(mockedRepositoriesManager.knownRepositories).thenReturn(setOf(repositoryMapping))
        Mockito.`when`(mockedAccountManager.accounts).thenReturn(setOf(account))
        Mockito.`when`(mockedAccountManager.findCredentials(account)).thenReturn("token")
        server.stubGetJsonResponse("/api/v4/projects/group%2Fproject", projectResponse)
    }

    @Test
    fun `test fetching projects`() {
        val sync = Object()
        synchronized(sync) {
            val service = GlProjectService(project)
            service.addListener(testRootDisposable) {
                sync.notify()
            }
            sync.wait(5000)
            assertNotEmpty(service.projects)
            assertEquals(
                "project-name",
                service.projects.single().getOrHandle { throw Error("was left: $it") }.name
            )
        }
    }
}
