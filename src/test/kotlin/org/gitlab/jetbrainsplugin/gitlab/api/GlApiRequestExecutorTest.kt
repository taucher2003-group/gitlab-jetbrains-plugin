package org.gitlab.jetbrainsplugin.gitlab.api

import com.github.tomakehurst.wiremock.client.WireMock
import com.github.tomakehurst.wiremock.junit.WireMockRule
import com.intellij.testFramework.fixtures.BasePlatformTestCase
import org.gitlab.jetbrainsplugin.settings.account.GlServerPath
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class GlApiRequestExecutorTest : BasePlatformTestCase() {
    val port = 8089
    lateinit var executor: GlApiRequestExecutor

    private val userResponse = """{ "name": "Josh", "username": "josh" }"""
    private val testToken = "test-token"

    @get:Rule
    var server = WireMockRule(port)

    @Before
    fun before() {
        executor = GlApiRequestExecutor.Factory.getInstance().create(testToken)
    }

    @Test
    fun `test submits token, accepted response and user agent`() {
        server.stubFor(
            WireMock.get(WireMock.urlEqualTo("/api/v4/user"))
                .withHeader("Authorization", WireMock.equalTo("Bearer $testToken"))
                .withHeader("Accept", WireMock.equalTo("application/json"))
                .withHeader("User-Agent", WireMock.equalTo("gitlab-jetbrains-plugin"))
                .willReturn(
                    WireMock.aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody(userResponse)
                )
        )

        val user = executor.execute(GitLabApiRequests.CurrentUser.get(GlServerPath("http://localhost:$port")))

        assertEquals("Josh", user.name)
    }
}
