package org.gitlab.jetbrainsplugin.gitlab.api

import com.github.tomakehurst.wiremock.junit.WireMockRule
import com.intellij.testFramework.fixtures.BasePlatformTestCase
import org.gitlab.jetbrainsplugin.gitlab.GlRepositoryCoordinates
import org.gitlab.jetbrainsplugin.gitlab.GlRepositoryPath
import org.gitlab.jetbrainsplugin.testutils.TestData.Responses.projectResponse
import org.gitlab.jetbrainsplugin.testutils.TestData.Responses.userResponse
import org.gitlab.jetbrainsplugin.testutils.TestData.port
import org.gitlab.jetbrainsplugin.testutils.TestData.serverPath
import org.gitlab.jetbrainsplugin.testutils.stubGetJsonResponse
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class GitLabApiRequestsTest : BasePlatformTestCase() {
    lateinit var executor: GlApiRequestExecutor

    @get:Rule
    var server = WireMockRule(port)

    @Before
    fun before() {
        executor = GlApiRequestExecutor.Factory.getInstance().create("some-token")
    }

    @Test
    fun `test getting user information`() {
        server.stubGetJsonResponse("/api/v4/user", userResponse)

        val user = executor.execute(GitLabApiRequests.CurrentUser.get(serverPath))

        assertEquals("Josh", user.name)
    }

    @Test
    fun `test getting project information`() {
        server.stubGetJsonResponse("/api/v4/projects/group%2Fproject", projectResponse)
        val repositoryCoordinates = GlRepositoryCoordinates(
            serverPath,
            GlRepositoryPath("group", "project")
        )

        val project = executor.execute(GitLabApiRequests.Project.get(repositoryCoordinates))

        assertEquals("project-name", project.name)
    }
}
