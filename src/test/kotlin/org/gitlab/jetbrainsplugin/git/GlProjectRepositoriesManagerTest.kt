package org.gitlab.jetbrainsplugin.git

import com.intellij.openapi.components.service
import com.intellij.testFramework.fixtures.BasePlatformTestCase
import com.intellij.testFramework.replaceService
import com.intellij.util.application
import git4idea.repo.GitRemote
import git4idea.repo.GitRepository
import git4idea.repo.GitRepositoryManager
import org.gitlab.jetbrainsplugin.settings.account.GlAccount
import org.gitlab.jetbrainsplugin.settings.account.GlAccountManager
import org.gitlab.jetbrainsplugin.settings.account.GlServerPath
import org.junit.Test
import org.mockito.Mockito

class GlProjectRepositoriesManagerTest : BasePlatformTestCase() {

    lateinit var mockedGitManager: GitRepositoryManager
    lateinit var mockedRepository: GitRepository
    lateinit var mockedAccountManager: GlAccountManager

    private val remoteUrl = "git@gitlab.com:gitlab-org/gitlab-jetbrains-plugin.git"
    private val testRemote = GitRemote("origin", listOf(remoteUrl), emptyList(), emptyList(), emptyList())

    override fun setUp() {
        super.setUp()
        mockedGitManager = Mockito.mock(GitRepositoryManager::class.java)
        mockedRepository = Mockito.mock(GitRepository::class.java)
        mockedAccountManager = Mockito.mock(GlAccountManager::class.java)
        project.replaceService(GitRepositoryManager::class.java, mockedGitManager, testRootDisposable)
        application.replaceService(GlAccountManager::class.java, mockedAccountManager, testRootDisposable)
    }

    private fun mockGitAndAccountAndRefreshManager() {
        Mockito.`when`(mockedGitManager.repositories).thenReturn(listOf(mockedRepository))
        Mockito.`when`(mockedRepository.remotes).thenReturn(listOf(testRemote))
        Mockito.`when`(mockedAccountManager.accounts).thenReturn(setOf(GlAccount("", GlServerPath.DEFAULT_SERVER)))

        GlProjectRepositoriesManager.RemoteUrlsListener(project).mappingChanged() // trigger update
    }

    private fun getManager() = project.service<GlProjectRepositoriesManager>()

    @Test
    fun `test recognises one repository`() {
        mockGitAndAccountAndRefreshManager()

        assertEquals(1, getManager().knownRepositories.size)
    }

    @Test
    fun `test repository mapping contains information for connecting to GitLab API`() {
        mockGitAndAccountAndRefreshManager()

        val repositoryMapping = getManager().knownRepositories.single()

        assertEquals(GlServerPath.DEFAULT_SERVER, repositoryMapping.glRepositoryCoordinates.serverPath)
        assertEquals(
            "gitlab-org/gitlab-jetbrains-plugin",
            repositoryMapping.glRepositoryCoordinates.repositoryPath.toString()
        )
    }

    @Test
    fun `test repository mapping contains pointer back to the correct remote URL`() {
        mockGitAndAccountAndRefreshManager()

        val repositoryMapping = getManager().knownRepositories.single()

        assertEquals(testRemote, repositoryMapping.gitRemoteUrlCoordinates.remote)
        assertEquals(mockedRepository, repositoryMapping.gitRemoteUrlCoordinates.repository)
        assertEquals(remoteUrl, repositoryMapping.gitRemoteUrlCoordinates.url)
    }
}
