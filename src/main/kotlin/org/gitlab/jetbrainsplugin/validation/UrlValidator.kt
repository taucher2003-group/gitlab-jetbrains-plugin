package org.gitlab.jetbrainsplugin.validation

import org.gitlab.jetbrainsplugin.GitLabBundle.message
import java.net.MalformedURLException
import java.net.URISyntaxException
import java.net.URL

object UrlValidator {
    /**
     * Returns validation error or null if there is no error
     */
    fun validateUrl(url: String): String? {
        if (!Regex("^https?://").containsMatchIn(url)) return message("validation.instanceUrl.https")
        try {
            URL(url).toURI()
        } catch (e: MalformedURLException) {
            return message("validation.instanceUrl.invalid", e.message.orEmpty())
        } catch (e: URISyntaxException) {
            return message("validation.instanceUrl.invalid", e.message.orEmpty())
        }
        return null
    }
}
