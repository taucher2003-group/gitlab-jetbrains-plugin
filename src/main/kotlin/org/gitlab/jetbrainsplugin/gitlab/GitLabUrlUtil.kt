/*
Copyright 2000-2021 JetBrains s.r.o.
Licensed under the Apache License, Version 2.0 (the "License").
You may not use this file except in compliance with the License.
You may obtain a copy of the License at www.apache.org/licenses/LICENSE-2.0.txt.
*/
package org.gitlab.jetbrainsplugin.gitlab

import com.intellij.collaboration.hosting.GitHostingUrlUtil.removeProtocolPrefix

/**
 * @author Aleksey Pivovarov
 */
object GitLabUrlUtil {
    /** Ignore everything to the first slash or colon, then split the rest by the last slash. */
    private val REMOTE_REGEX = Regex("^[^/:]+[/:](.+)/([^/]+)$")

    /**
     * git@gitlab.com:group/subgroup/repo.git -> group/subgroup, repo
     */
    fun getNamespaceAndProjectFromRemoteUrl(remoteUrl: String): GlRepositoryPath? {
        val normalizedUrl = removeProtocolPrefix(removeEndingDotGit(remoteUrl))
        val (namespace, project) = REMOTE_REGEX.matchEntire(normalizedUrl)?.destructured ?: return null

        return GlRepositoryPath(namespace, project)
    }

    private fun removeEndingDotGit(url: String) = url.replace("(.git)?/?$".toRegex(), "")
}
