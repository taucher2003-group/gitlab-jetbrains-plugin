/*
Copyright 2000-2021 JetBrains s.r.o.
Licensed under the Apache License, Version 2.0 (the "License").
You may not use this file except in compliance with the License.
You may obtain a copy of the License at www.apache.org/licenses/LICENSE-2.0.txt.
*/
package org.gitlab.jetbrainsplugin.gitlab.api

import com.intellij.openapi.components.service
import com.intellij.util.concurrency.annotations.RequiresEdt
import org.gitlab.jetbrainsplugin.settings.account.GlAccount
import org.gitlab.jetbrainsplugin.settings.account.GlAccountManager
import vendor.org.jetbrains.plugins.github.exceptions.GithubMissingTokenException

/**
 * Allows acquiring API executor without exposing the auth token to external code
 */
class GlApiRequestExecutorManager {
    private val executors = mutableMapOf<GlAccount, GlApiRequestExecutor.WithTokenAuth>()

    companion object {
        @JvmStatic
        fun getInstance(): GlApiRequestExecutorManager = service()
    }

    internal fun tokenChanged(account: GlAccount) {
        val token = service<GlAccountManager>().findCredentials(account)
        if (token == null) executors.remove(account)
        else executors[account]?.token = token
    }

//    @RequiresEdt
//    fun getExecutor(account: GlAccount, project: Project): GlApiRequestExecutor.WithTokenAuth? {
//        return getOrTryToCreateExecutor(account) {
//            GithubAuthenticationManager.getInstance().requestNewToken(account, project)
//        }
//    }
//
//    @RequiresEdt
//    fun getExecutor(account: GlAccount, parentComponent: Component): GlApiRequestExecutor.WithTokenAuth? {
//        return getOrTryToCreateExecutor(account) {
//            GithubAuthenticationManager.getInstance().requestNewToken(account, null, parentComponent)
//        }
//    }

    @RequiresEdt
    @Throws(GithubMissingTokenException::class)
    fun getExecutor(account: GlAccount): GlApiRequestExecutor.WithTokenAuth {
        return getOrTryToCreateExecutor(account) { throw GithubMissingTokenException(account) }!!
    }

    private fun getOrTryToCreateExecutor(
        account: GlAccount,
        missingTokenHandler: () -> String?
    ): GlApiRequestExecutor.WithTokenAuth? {

        return executors.getOrPut(account) {
            service<GlAccountManager>().findCredentials(account)
                ?.let(GlApiRequestExecutor.Factory.getInstance()::create) ?: return null
        }
    }
}
