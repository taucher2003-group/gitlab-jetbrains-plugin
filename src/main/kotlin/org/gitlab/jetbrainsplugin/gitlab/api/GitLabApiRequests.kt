/*
Copyright 2000-2021 JetBrains s.r.o.
Licensed under the Apache License, Version 2.0 (the "License").
You may not use this file except in compliance with the License.
You may obtain a copy of the License at www.apache.org/licenses/LICENSE-2.0.txt.
*/
package org.gitlab.jetbrainsplugin.gitlab.api

import org.gitlab.jetbrainsplugin.gitlab.GlRepositoryCoordinates
import org.gitlab.jetbrainsplugin.gitlab.api.data.GlProject
import org.gitlab.jetbrainsplugin.gitlab.api.data.GlUser
import org.gitlab.jetbrainsplugin.settings.account.GlServerPath
import vendor.org.jetbrains.plugins.github.api.GithubApiRequest.Get
import java.net.URLEncoder

object GitLabApiRequests {
    object CurrentUser : Entity("/user") {
        fun get(server: GlServerPath) = get(getUrl(server, urlSuffix))

        fun get(url: String) =
            Get.json<GlUser>(url, "application/json").withOperationName("get profile information")
    }

    object Project : Entity("/projects") {
        fun get(repository: GlRepositoryCoordinates) =
            get(getUrl(repository, "/", URLEncoder.encode(repository.repositoryPath.toString(), "utf-8")))

        fun get(url: String) =
            Get.json<GlProject>(url, "application/json").withOperationName("get project")
    }

    abstract class Entity(val urlSuffix: String)

    private fun getUrl(server: GlServerPath, suffix: String) = server.toApiUrl() + suffix

    private fun getUrl(repository: GlRepositoryCoordinates, vararg suffixes: String) =
        getUrl(repository.serverPath, Project.urlSuffix, *suffixes)

    fun getUrl(server: GlServerPath, vararg suffixes: String) =
        StringBuilder(server.toApiUrl()).append(*suffixes).toString()
}
