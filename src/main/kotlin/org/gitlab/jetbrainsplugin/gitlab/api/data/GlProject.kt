package org.gitlab.jetbrainsplugin.gitlab.api.data

data class GlProject(val id: Int, val name: String)
