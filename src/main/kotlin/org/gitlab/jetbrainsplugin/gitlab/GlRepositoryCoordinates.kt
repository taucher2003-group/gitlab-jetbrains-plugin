/*
Copyright 2000-2022 JetBrains s.r.o.
Licensed under the Apache License, Version 2.0 (the "License").
You may not use this file except in compliance with the License.
You may obtain a copy of the License at www.apache.org/licenses/LICENSE-2.0.txt.
*/
package org.gitlab.jetbrainsplugin.gitlab

import com.intellij.openapi.util.NlsSafe
import org.gitlab.jetbrainsplugin.settings.account.GlServerPath

// TODO rename to GlProjectCoordinates
/**
 * GlRepositoryCoordinates contains all information to connect to GitLab API (server + namespace + project)
 */
data class GlRepositoryCoordinates(val serverPath: GlServerPath, val repositoryPath: GlRepositoryPath) {
    fun toUrl(): String {
        return serverPath.toUrl() + "/" + repositoryPath
    }

    @NlsSafe
    override fun toString(): String {
        return "$serverPath/$repositoryPath"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is GlRepositoryCoordinates) return false

        if (!serverPath.equals(other.serverPath, true)) return false
        if (repositoryPath != other.repositoryPath) return false

        return true
    }

    override fun hashCode(): Int {
        var result = serverPath.hashCode()
        result = 31 * result + repositoryPath.hashCode()
        return result
    }
}
