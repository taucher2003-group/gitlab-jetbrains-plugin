/*
Copyright 2000-2022 JetBrains s.r.o.
Licensed under the Apache License, Version 2.0 (the "License").
You may not use this file except in compliance with the License.
You may obtain a copy of the License at www.apache.org/licenses/LICENSE-2.0.txt.
*/
package org.gitlab.jetbrainsplugin.gitlab

import com.intellij.openapi.util.NlsSafe
import com.intellij.openapi.util.text.StringUtil

class GlRepositoryPath(val owner: String, val repository: String) {

    fun toString(showOwner: Boolean) = if (showOwner) "$owner/$repository" else repository

    @NlsSafe
    override fun toString() = "$owner/$repository"

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is GlRepositoryPath) return false

        if (!owner.equals(other.owner, true)) return false
        if (!repository.equals(other.repository, true)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = StringUtil.stringHashCodeInsensitive(owner)
        result = 31 * result + StringUtil.stringHashCodeInsensitive(repository)
        return result
    }
}
