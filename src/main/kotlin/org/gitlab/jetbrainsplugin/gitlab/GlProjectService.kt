package org.gitlab.jetbrainsplugin.gitlab

import com.intellij.collaboration.async.CompletableFutureUtil.submitIOTask
import com.intellij.openapi.Disposable
import com.intellij.openapi.application.ModalityState
import com.intellij.openapi.components.Service
import com.intellij.openapi.components.service
import com.intellij.openapi.progress.EmptyProgressIndicator
import com.intellij.openapi.progress.ProgressIndicator
import com.intellij.openapi.progress.ProgressManager
import com.intellij.openapi.project.Project
import com.intellij.openapi.util.Disposer
import com.intellij.remoteDev.util.createSubProgress
import org.gitlab.jetbrainsplugin.GitLabBundle.message
import org.gitlab.jetbrainsplugin.git.GlGitRepositoryMapping
import org.gitlab.jetbrainsplugin.git.GlProjectRepositoriesManager
import org.gitlab.jetbrainsplugin.gitlab.api.GitLabApiRequests
import org.gitlab.jetbrainsplugin.gitlab.api.GlApiRequestExecutor
import org.gitlab.jetbrainsplugin.gitlab.api.data.GlProject
import org.gitlab.jetbrainsplugin.settings.account.GlAccountManager
import org.gitlab.jetbrainsplugin.utils.Either
import org.gitlab.jetbrainsplugin.utils.Left
import org.gitlab.jetbrainsplugin.utils.Right
import java.util.concurrent.CompletableFuture
import java.util.concurrent.CopyOnWriteArrayList
import kotlin.properties.Delegates

class ProjectLoadError(repository: GlGitRepositoryMapping, message: String) :
    Exception("${repository.repositoryPath} ($message)")

fun interface ProjectsUpdateListener {
    fun projectsUpdated(projects: List<ProjectResult>)
}

typealias ProjectResult = Either<Throwable, GlProject>

@Service(Service.Level.PROJECT)
class GlProjectService(private val project: Project) : Disposable {
    private val disposable = Disposer.newDisposable()
    private val accountManager: GlAccountManager
        get() = service()
    private val listeners = CopyOnWriteArrayList<ProjectsUpdateListener>()
    var projects: List<ProjectResult> by Delegates.observable(listOf()) { _, _, newProjects ->
        listeners.forEach { it.projectsUpdated(newProjects) }
    }
        private set

    init {
        val repositoriesManager = project.service<GlProjectRepositoriesManager>()
        repositoriesManager.addRepositoryListChangedListener(disposable) { refresh() }
        accountManager.addListener(disposable, GlAccountManager.createSimpleListener { refresh() })
        refresh()
    }

    private fun getTokenForRemote(repositoryAndRemote: GlGitRepositoryMapping): String {
        val firstRemote = repositoryAndRemote.gitRemote.firstUrl
            ?: throw ProjectLoadError(repositoryAndRemote, message("project.service.no.remotes"))
        val matchingAccount = accountManager.accounts.firstOrNull { it.server.matches(firstRemote) }
            ?: throw ProjectLoadError(repositoryAndRemote, message("project.service.no.account"))
        val token = accountManager.findCredentials(matchingAccount)
        checkNotNull(token) { "Account $matchingAccount is missing credentials" }
        return token
    }

    private fun turnRemoteIntoProject(
        repositoryAndRemote: GlGitRepositoryMapping,
        progressIndicator: ProgressIndicator
    ): CompletableFuture<GlProject> = service<ProgressManager>()
        .submitIOTask(progressIndicator) {
            val token = getTokenForRemote(repositoryAndRemote)
            val executor = GlApiRequestExecutor.Factory.getInstance().create(token)
            executor.execute(it, GitLabApiRequests.Project.get(repositoryAndRemote.glRepositoryCoordinates))
        }

    private fun getAllProjects(indicator: ProgressIndicator): CompletableFuture<List<ProjectResult>> {
        val repositoriesManager = project.service<GlProjectRepositoriesManager>()
        val projectFutures = repositoriesManager.knownRepositories.map {
            turnRemoteIntoProject(
                it,
                indicator.createSubProgress(1.0 / repositoriesManager.knownRepositories.size)
            ).handle { proj, err ->
                if (err == null) Right(proj) else Left(
                    // using err.cause, so we don't wrap the original error in the CompletionException
                    err.cause ?: err
                )
            }
        }
        return CompletableFuture.allOf(*projectFutures.toTypedArray()).thenApply { projectFutures.map { it.get() } }
    }

    fun refresh() {
        val modalityState = ModalityState.NON_MODAL
        val emptyProgressIndicator = EmptyProgressIndicator(modalityState)
        Disposer.register(disposable) { emptyProgressIndicator.cancel() }
        getAllProjects(emptyProgressIndicator)
            .thenApply { projects = it }
    }

    fun addListener(disposable: Disposable, listener: ProjectsUpdateListener) {
        listeners.add(listener)
        Disposer.register(disposable) {
            listeners.remove(listener)
        }
    }

    override fun dispose() {
        disposable.dispose()
    }
}
