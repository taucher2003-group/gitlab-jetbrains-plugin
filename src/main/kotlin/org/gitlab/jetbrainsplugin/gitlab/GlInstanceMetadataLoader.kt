/*
Copyright 2000-2022 JetBrains s.r.o.
Licensed under the Apache License, Version 2.0 (the "License").
You may not use this file except in compliance with the License.
You may obtain a copy of the License at www.apache.org/licenses/LICENSE-2.0.txt.
*/
package org.gitlab.jetbrainsplugin.gitlab

import com.intellij.openapi.Disposable
import com.intellij.openapi.components.Service
import org.gitlab.jetbrainsplugin.settings.account.GlServerPath
import org.jetbrains.annotations.CalledInAny
import java.util.concurrent.CompletableFuture

@Service
class GlInstanceMetadataLoader : Disposable {

    @CalledInAny
    fun loadMetadata(server: GlServerPath): CompletableFuture<GlInstanceMetaData> {
        require(!server.isDefault()) { "Cannot retrieve metadata from gitlab.com" }
        // TODO implement API request
        return CompletableFuture.completedFuture(GlInstanceMetaData("14.8.0"))
    }

    override fun dispose() {}
}
