/*
Copyright 2000-2021 JetBrains s.r.o.
Licensed under the Apache License, Version 2.0 (the "License").
You may not use this file except in compliance with the License.
You may obtain a copy of the License at www.apache.org/licenses/LICENSE-2.0.txt.
*/
package org.gitlab.jetbrainsplugin.git

import com.intellij.collaboration.async.CompletableFutureUtil.errorOnEdt
import com.intellij.collaboration.async.CompletableFutureUtil.successOnEdt
import com.intellij.collaboration.auth.AccountsListener
import com.intellij.collaboration.hosting.GitHostingUrlUtil
import com.intellij.dvcs.repo.VcsRepositoryMappingListener
import com.intellij.openapi.Disposable
import com.intellij.openapi.application.ApplicationManager
import com.intellij.openapi.application.invokeLater
import com.intellij.openapi.application.runInEdt
import com.intellij.openapi.components.Service
import com.intellij.openapi.components.service
import com.intellij.openapi.diagnostic.logger
import com.intellij.openapi.project.Project
import com.intellij.util.concurrency.annotations.RequiresEdt
import com.intellij.util.messages.Topic
import com.intellij.util.ui.update.MergingUpdateQueue
import com.intellij.util.ui.update.Update
import git4idea.repo.GitRepository
import git4idea.repo.GitRepositoryChangeListener
import git4idea.repo.GitRepositoryManager
import org.gitlab.jetbrainsplugin.gitlab.GlInstanceMetadataLoader
import org.gitlab.jetbrainsplugin.settings.account.GlAccount
import org.gitlab.jetbrainsplugin.settings.account.GlAccountManager
import org.gitlab.jetbrainsplugin.settings.account.GlServerPath
import org.jetbrains.annotations.CalledInAny
import kotlin.properties.Delegates.observable

@Service
class GlProjectRepositoriesManager(private val project: Project) : Disposable {

    private val updateQueue = MergingUpdateQueue("GitLab repositories update", 50, true, null, this, null, true)
        .usePassThroughInUnitTestMode()

    private val accountManager: GlAccountManager
        get() = service()

    var knownRepositories by observable(emptySet<GlGitRepositoryMapping>()) { _, _, newValue ->
        project.messageBus.syncPublisher(LIST_CHANGES_TOPIC).onRepositoryListChanges(newValue)
    }
        private set

    private val serversFromDiscovery = HashSet<GlServerPath>()

    init {
        accountManager.addListener(
            this,
            object : AccountsListener<GlAccount> {
                override fun onAccountListChanged(old: Collection<GlAccount>, new: Collection<GlAccount>) = runInEdt {
                    updateRepositories()
                }
            }
        )
        updateRepositories()
    }

    @CalledInAny
    private fun updateRepositories() {
        updateQueue.queue(Update.create(UPDATE_IDENTITY, ::doUpdateRepositories))
    }

    // TODO: execute on pooled thread - need to make GlAccountManager ready
    @RequiresEdt
    private fun doUpdateRepositories() {
        LOG.debug("Repository list update started")
        val gitRepositories = project.service<GitRepositoryManager>().repositories
        if (gitRepositories.isEmpty()) {
            knownRepositories = emptySet()
            LOG.debug("No repositories found")
            return
        }

        val remoteUrlCoordinates = gitRepositories.flatMap { repo ->
            repo.remotes.flatMap { remote ->
                remote.urls.mapNotNull { url ->
                    GitRemoteUrlCoordinates(url, remote, repo)
                }
            }
        }
        LOG.debug("Found remotes: $remoteUrlCoordinates")

        val authenticatedServers = accountManager.accounts.map { it.server }
        val servers = mutableListOf<GlServerPath>().apply {
            add(GlServerPath.DEFAULT_SERVER)
            addAll(authenticatedServers)
            addAll(serversFromDiscovery)
        }

        val repositories = HashSet<GlGitRepositoryMapping>()
        for (remote in remoteUrlCoordinates) {
            val repository = servers.find { it.matches(remote.url) }?.let { GlGitRepositoryMapping.create(it, remote) }
            if (repository != null) repositories.add(repository)
            else {
                scheduleEnterpriseServerDiscovery(remote)
            }
        }
        LOG.debug("New list of known repos: $repositories")
        knownRepositories = repositories
    }

    @RequiresEdt
    private fun scheduleEnterpriseServerDiscovery(remote: GitRemoteUrlCoordinates) {
        val uri = GitHostingUrlUtil.getUriFromRemoteUrl(remote.url)
        LOG.debug("Extracted URI $uri from remote ${remote.url}")
        if (uri == null) return

        val host = uri.host ?: return

        val server = GlServerPath("https://$host")
        val serverHttp = GlServerPath("http://$host")
        LOG.debug("Scheduling GitLab instance discovery for $server, $serverHttp")

        val serverManager = service<GlInstanceMetadataLoader>()
        serverManager.loadMetadata(server).successOnEdt {
            LOG.debug("Found GitLab instance at $server")
            serversFromDiscovery.add(server)
            invokeLater(runnable = ::doUpdateRepositories)
        }.errorOnEdt {
            serverManager.loadMetadata(serverHttp).successOnEdt {
                LOG.debug("Found GitLab instance at $serverHttp")
                serversFromDiscovery.add(serverHttp)
                invokeLater(runnable = ::doUpdateRepositories)
            }
        }
    }

    fun addRepositoryListChangedListener(disposable: Disposable, listener: () -> Unit) =
        project.messageBus.connect(disposable).subscribe(
            LIST_CHANGES_TOPIC,
            object : ListChangesListener {
                override fun onRepositoryListChanges(newList: Set<GlGitRepositoryMapping>) = listener()
            }
        )

    class RemoteUrlsListener(private val project: Project) : VcsRepositoryMappingListener, GitRepositoryChangeListener {
        override fun mappingChanged() = runInEdt(project) { updateRepositories(project) }
        override fun repositoryChanged(repository: GitRepository) = runInEdt(project) { updateRepositories(project) }
    }

    interface ListChangesListener {
        fun onRepositoryListChanges(newList: Set<GlGitRepositoryMapping>)
    }

    companion object {
        private val LOG = logger<GlProjectRepositoriesManager>()

        private val UPDATE_IDENTITY = Any()

        @JvmField
        // project level topic
        val LIST_CHANGES_TOPIC = Topic.create("Repository List Changes", ListChangesListener::class.java)

        private inline fun runInEdt(project: Project, crossinline runnable: () -> Unit) {
            val application = ApplicationManager.getApplication()
            if (application.isDispatchThread) runnable()
            else application.invokeLater({ runnable() }) { project.isDisposed }
        }

        private fun updateRepositories(project: Project) {
            try {
                if (!project.isDisposed) project.service<GlProjectRepositoriesManager>().updateRepositories()
            } catch (e: Exception) {
                LOG.info("Error occurred while updating repositories", e)
            }
        }
    }

    override fun dispose() {}
}
