/*
Copyright 2000-2021 JetBrains s.r.o.
Licensed under the Apache License, Version 2.0 (the "License").
You may not use this file except in compliance with the License.
You may obtain a copy of the License at www.apache.org/licenses/LICENSE-2.0.txt.
*/
package org.gitlab.jetbrainsplugin.git

import com.intellij.openapi.util.NlsSafe
import git4idea.repo.GitRemote
import git4idea.repo.GitRepository

class GitRemoteUrlCoordinates(@NlsSafe val url: String, val remote: GitRemote, val repository: GitRepository) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is GitRemoteUrlCoordinates) return false

        if (url != other.url) return false

        return true
    }

    override fun hashCode(): Int {
        return url.hashCode()
    }

    override fun toString(): String {
        return "(url='$url', remote=${remote.name}, repository=$repository)"
    }
}
