/*
Copyright 2000-2021 JetBrains s.r.o.
Licensed under the Apache License, Version 2.0 (the "License").
You may not use this file except in compliance with the License.
You may obtain a copy of the License at www.apache.org/licenses/LICENSE-2.0.txt.
*/
package org.gitlab.jetbrainsplugin.git

import git4idea.repo.GitRemote
import git4idea.repo.GitRepository
import git4idea.ui.branch.GitRepositoryMappingData
import org.gitlab.jetbrainsplugin.gitlab.GitLabUrlUtil
import org.gitlab.jetbrainsplugin.gitlab.GlRepositoryCoordinates
import org.gitlab.jetbrainsplugin.settings.account.GlServerPath

/**
 * GlGitRepositoryMapping contains information about how to connect to the GitLab instance (glRepositoryCoordinates)
 * and how to find the correct GitLab project on that instance (gitRemoteUrlCoordinates).
 */
class GlGitRepositoryMapping(
    val glRepositoryCoordinates: GlRepositoryCoordinates,
    val gitRemoteUrlCoordinates: GitRemoteUrlCoordinates
) : GitRepositoryMappingData {
    override val gitRemote: GitRemote
        get() = gitRemoteUrlCoordinates.remote
    override val gitRepository: GitRepository
        get() = gitRemoteUrlCoordinates.repository
    override val repositoryPath: String
        get() = glRepositoryCoordinates.repositoryPath.repository

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is GlGitRepositoryMapping) return false

        if (glRepositoryCoordinates != other.glRepositoryCoordinates) return false

        return true
    }

    override fun hashCode(): Int {
        return glRepositoryCoordinates.hashCode()
    }

    override fun toString(): String {
        return "(repository=$glRepositoryCoordinates, remote=$gitRemoteUrlCoordinates)"
    }

    companion object {
        fun create(server: GlServerPath, remote: GitRemoteUrlCoordinates): GlGitRepositoryMapping? {
            val repositoryPath = GitLabUrlUtil.getNamespaceAndProjectFromRemoteUrl(remote.url) ?: return null
            val repository = GlRepositoryCoordinates(server, repositoryPath)
            return GlGitRepositoryMapping(repository, remote)
        }
    }
}
