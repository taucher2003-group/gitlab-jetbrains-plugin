package org.gitlab.jetbrainsplugin.toolwindow

import com.intellij.icons.AllIcons
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import com.intellij.openapi.wm.ToolWindow
import com.intellij.ui.components.Panel
import com.intellij.ui.layout.panel
import com.intellij.ui.treeStructure.Tree
import com.intellij.util.application
import org.gitlab.jetbrainsplugin.GitLabBundle.message
import org.gitlab.jetbrainsplugin.gitlab.GlProjectService
import org.gitlab.jetbrainsplugin.gitlab.ProjectResult
import org.gitlab.jetbrainsplugin.utils.getOrHandle
import javax.swing.tree.DefaultMutableTreeNode
import javax.swing.tree.DefaultTreeCellRenderer

private fun makeProjectNode(result: ProjectResult) =
    DefaultMutableTreeNode(result.map { it.name }.getOrHandle { it.message })

private fun buildProjectTree(results: List<ProjectResult>): Tree {
    val root = DefaultMutableTreeNode()
    val projectNodes = results.map(::makeProjectNode)
    projectNodes.forEach { root.add(it) }
    val renderer = DefaultTreeCellRenderer().apply { leafIcon = AllIcons.Actions.ProjectDirectory }
    val tree = Tree(root).apply {
        isRootVisible = false
        cellRenderer = renderer
    }
    return tree
}

private fun buildEmptyLabel() =
    panel {
        noteRow(message("gitlab.toolwindow.no.repositories"))
    }

class GitLabToolWindow(private val project: Project, toolWindow: ToolWindow) {

    private val panel = Panel()
    private val glProjectService: GlProjectService
        get() = project.service()

    init {
        val contentManager = toolWindow.contentManager
        val content = contentManager.factory.createContent(panel, "", true)
        contentManager.addContent(content)
        renderProjects(glProjectService.projects)
        glProjectService.addListener(toolWindow.disposable) { application.invokeLater { renderProjects(it) } }
    }

    private fun renderProjects(results: List<ProjectResult>) {
        panel.removeAll()
        if (results.isEmpty()) panel.add(buildEmptyLabel())
        else panel.add(buildProjectTree(results))
    }
}
