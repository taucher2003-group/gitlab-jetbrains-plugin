/*
Copyright 2000-2021 JetBrains s.r.o.
Licensed under the Apache License, Version 2.0 (the "License").
You may not use this file except in compliance with the License.
You may obtain a copy of the License at www.apache.org/licenses/LICENSE-2.0.txt.
*/
package org.gitlab.jetbrainsplugin.settings.account

import com.intellij.collaboration.async.CompletableFutureUtil.submitIOTask
import com.intellij.collaboration.async.CompletableFutureUtil.successOnEdt
import com.intellij.collaboration.auth.ui.LoadingAccountsDetailsProvider
import com.intellij.collaboration.util.ProgressIndicatorsProvider
import com.intellij.openapi.application.ModalityState
import com.intellij.openapi.components.service
import com.intellij.openapi.progress.EmptyProgressIndicator
import com.intellij.openapi.progress.ProgressIndicator
import com.intellij.openapi.progress.ProgressManager
import org.gitlab.jetbrainsplugin.GitLabBundle
import org.gitlab.jetbrainsplugin.gitlab.api.GitLabApiRequests
import org.gitlab.jetbrainsplugin.gitlab.api.GlApiRequestExecutor
import org.gitlab.jetbrainsplugin.gitlab.api.data.GlUser
import java.util.concurrent.CompletableFuture

internal class GlAccountsDetailsProvider(
    progressIndicatorsProvider: ProgressIndicatorsProvider,
    private val accountManager: GlAccountManager,
    private val accountsModel: GlAccountsListModel
) : LoadingAccountsDetailsProvider<GlAccount, GlUser>(progressIndicatorsProvider) {

    override fun scheduleLoad(
        account: GlAccount,
        indicator: ProgressIndicator
    ): CompletableFuture<DetailsLoadingResult<GlUser>> {
        val token = accountsModel.newCredentials.getOrElse(account) {
            accountManager.findCredentials(account)
        } ?: return CompletableFuture.completedFuture(noToken())
        val executor = service<GlApiRequestExecutor.Factory>().create(token)
        return ProgressManager.getInstance().submitIOTask(EmptyProgressIndicator()) {
            val details = executor.execute(GitLabApiRequests.CurrentUser.get(account.server))
            // TODO avatars
            DetailsLoadingResult<GlUser>(details, null, null, false) // TODO avatar
        }.successOnEdt(ModalityState.any()) {
            accountsModel.accountsListModel.contentsChanged(account)
            it
        }
    }

    private fun noToken() = DetailsLoadingResult<GlUser>(
        null,
        null,
        GitLabBundle.message("account.token.missing"),
        true
    )
}
