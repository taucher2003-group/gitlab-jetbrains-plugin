/*
Copyright 2000-2018 JetBrains s.r.o.
Licensed under the Apache License, Version 2.0 (the "License").
You may not use this file except in compliance with the License.
You may obtain a copy of the License at www.apache.org/licenses/LICENSE-2.0.txt.
*/

package org.gitlab.jetbrainsplugin.settings.account

import com.intellij.collaboration.auth.ServerAccount
import com.intellij.util.xmlb.annotations.Attribute
import com.intellij.util.xmlb.annotations.Property
import com.intellij.util.xmlb.annotations.Tag
import com.intellij.util.xmlb.annotations.Transient
import org.jetbrains.annotations.VisibleForTesting

@Tag("account")
class GlAccount(
    @get:Transient
    override var name: String = "",
    @Property(style = Property.Style.ATTRIBUTE, surroundWithTag = false)
    override var server: GlServerPath = GlServerPath(),
    @Attribute
    @VisibleForTesting
    override var id: String = generateId(),
) : ServerAccount()
