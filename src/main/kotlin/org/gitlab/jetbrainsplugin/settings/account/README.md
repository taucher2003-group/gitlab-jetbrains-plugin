# IntelliJ account model

To create the UI for account settings, we have to call the

```kotlin
AccountsPanelFactory.accountsPanel(
    AccountManager,
    PersistentDefaultAccountHolder,
    AccountsListModel,
    AccountsDetailsProvider
)
```

You can find this call in `GitLabProjectSettings.kt`.

## Class diagram

The following diagram shows all the interfaces/abstract classes that must be implemented for functioning account
settings.

- `AccountsListModel` - UI model that backs the Accounts Settings Panel
- `AccountDetailsProvider` - Used for (asynchronous) fetching of account details (e.g. avatar and username)
- `PersistentDefaultAccountHolder` - Project-level settings that keep user's preferred account
- `AccountManager` - Key service, it's used as a model for all Accounts
- `AccountRepository` - Interface that must be implemented by a persistent setting service so the added accounts are
  persisted

```mermaid
classDiagram
    class AccountDetails {
        String name
    }
    class AccountManager{
        findCredentials()
        updateAccount()
        deleteAccount()
        addListener()
        accountsRepository() AccountsRepository
    }
    class AccountsListModelBase{
        accounts: Set~Account~
        defaultAccount: Account
        newCredentials: Map~Account,Cred~
        accountListModel
        busyStateModel
        addAccount(parentComponent, point)
        editAccount(parentComponent, account)
    }
    AccountManager ..> AccountRepository
    class AccountRepository {
        accounts: Set~Account~
    }
    PersistentDefaultAccountHolder ..> AccountManager
    class PersistentDefaultAccountHolder {
        Account account
        accountManger() AccountManger
        notifyDefaultAccountMissing()
    }
    AccountsDetailsProvider ..> AccountDetails
    class AccountsDetailsProvider {
        <<interface>>
        loadingStateModel SingleValueModel<Boolean>
        getDetails(account) D
        getAvatarImage(account) Image
        getErrorText(account) String
        checkErrorRequiresReLogin(account) Boolean
        reset(account)
        resetAll()
    }
```
