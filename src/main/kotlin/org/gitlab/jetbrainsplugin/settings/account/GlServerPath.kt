package org.gitlab.jetbrainsplugin.settings.account

import com.intellij.collaboration.api.ServerPath
import com.intellij.collaboration.hosting.GitHostingUrlUtil
import com.intellij.util.Urls.parseEncoded
import com.intellij.util.xmlb.annotations.Attribute
import com.intellij.util.xmlb.annotations.Tag
import vendor.org.jetbrains.plugins.github.exceptions.GithubParseException
import java.net.URI

@Tag("server")
class GlServerPath(
    @Attribute
    var instanceUrl: String = ""
) : ServerPath {
    fun toUrl() = instanceUrl

    fun toApiUrl() = "$instanceUrl/api/v4"

    override fun toString(): String {
        return instanceUrl.replace(Regex("https?://"), "")
    }

    fun matches(gitRemoteUrl: String): Boolean =
        GitHostingUrlUtil.getUriFromRemoteUrl(gitRemoteUrl)?.host == URI(instanceUrl).host

    fun isDefault(): Boolean = this == DEFAULT_SERVER

    fun isValid(): Boolean = parseEncoded(instanceUrl) != null

    fun equals(o: Any?, ignoreProtocol: Boolean): Boolean = TODO("can't check equality without protocol yet")

    companion object {
        const val DEFAULT_HOST = "https://gitlab.com"
        val DEFAULT_SERVER = GlServerPath(DEFAULT_HOST)

        fun from(url: String): GlServerPath {
            val path = GlServerPath(url)
            if (!path.isValid()) throw GithubParseException("Not a valid URL")
            return path
        }
    }
}
