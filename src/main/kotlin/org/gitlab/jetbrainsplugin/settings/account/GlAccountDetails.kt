package org.gitlab.jetbrainsplugin.settings.account

import com.intellij.collaboration.auth.AccountDetails

data class GlAccountDetails(override val name: String) : AccountDetails
