package org.gitlab.jetbrainsplugin.settings.account

import com.intellij.collaboration.auth.AccountManager
import com.intellij.collaboration.auth.PersistentDefaultAccountHolder
import com.intellij.openapi.components.Service
import com.intellij.openapi.components.State
import com.intellij.openapi.components.Storage
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project

@Service(Service.Level.PROJECT)
@State(name = "DefaultGitLabAccount", storages = [Storage("gitlab.xml")])
class GlDefaultAccountHolder(project: Project) : PersistentDefaultAccountHolder<GlAccount>(project) {
    override fun accountManager(): AccountManager<GlAccount, *> {
        return service<GlAccountManager>()
    }

    override fun notifyDefaultAccountMissing() {
        // TODO("Not yet implemented")
    }
}
