package org.gitlab.jetbrainsplugin.settings.account

import com.intellij.collaboration.auth.AccountsRepository
import com.intellij.openapi.components.PersistentStateComponent
import com.intellij.openapi.components.Service
import com.intellij.openapi.components.State
import com.intellij.openapi.components.Storage

@Service(Service.Level.APP)
@State(name = "GitLabAccounts", storages = [Storage("gitlab.xml")])
class GlAccountsRepository : AccountsRepository<GlAccount>, PersistentStateComponent<Array<GlAccount>> {
    private var state: Array<GlAccount> = emptyArray()

    override var accounts: Set<GlAccount>
        get() = state.toSet()
        set(value) {
            state = value.toTypedArray()
        }

    override fun getState(): Array<GlAccount> {
        return state
    }

    override fun loadState(newState: Array<GlAccount>) {
        state = newState
    }
}
