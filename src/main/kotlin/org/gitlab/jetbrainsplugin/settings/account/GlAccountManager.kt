package org.gitlab.jetbrainsplugin.settings.account

import com.intellij.collaboration.auth.AccountManagerBase
import com.intellij.collaboration.auth.AccountsListener
import com.intellij.collaboration.auth.AccountsRepository
import com.intellij.openapi.components.Service
import com.intellij.openapi.components.service
import org.gitlab.jetbrainsplugin.Constants

@Service(Service.Level.APP)
class GlAccountManager : AccountManagerBase<GlAccount, String>(Constants.SERVICE_NAME) {
    override fun accountsRepository(): AccountsRepository<GlAccount> {
        return service<GlAccountsRepository>()
    }

    override fun deserializeCredentials(credentials: String): String = credentials

    override fun serializeCredentials(credentials: String): String = credentials

    companion object {
        fun createAccount(name: String, server: GlServerPath) = GlAccount(name, server)
        fun createSimpleListener(listener: () -> Unit): AccountsListener<GlAccount> =
            object : AccountsListener<GlAccount> {
                override fun onAccountCredentialsChanged(account: GlAccount) = listener()
                override fun onAccountListChanged(old: Collection<GlAccount>, new: Collection<GlAccount>) = listener()
            }
    }
}
