package org.gitlab.jetbrainsplugin.settings.account

import com.intellij.collaboration.auth.ui.AccountsListModelBase
import com.intellij.openapi.actionSystem.ActionGroup
import com.intellij.openapi.actionSystem.ActionManager
import com.intellij.openapi.actionSystem.ActionPlaces
import com.intellij.openapi.components.Service
import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.JBPopupMenu
import com.intellij.ui.awt.RelativePoint
import vendor.org.jetbrains.plugins.github.authentication.ui.GHAccountsHost
import javax.swing.JComponent

@Service(Service.Level.PROJECT)
class GlAccountsListModel(private val project: Project) : AccountsListModelBase<GlAccount, String>(), GHAccountsHost {
    private val actionManager = ActionManager.getInstance()

    override fun addAccount(parentComponent: JComponent, point: RelativePoint?) {
        val group = actionManager.getAction("GitLab.Accounts.AddAccount") as ActionGroup
        val popup = actionManager.createActionPopupMenu(ActionPlaces.UNKNOWN, group)

        val actualPoint = point ?: RelativePoint.getCenterOf(parentComponent)
        popup.setTargetComponent(parentComponent)
        JBPopupMenu.showAt(actualPoint, popup.component)
    }

    override fun addAccount(server: GlServerPath, login: String, token: String) {
        val account = GlAccountManager.createAccount(login, server)
        accountsListModel.add(account)
        newCredentials[account] = token
        notifyCredentialsChanged(account)
    }

    override fun isAccountUnique(login: String, server: GlServerPath): Boolean =
        accountsListModel.items.none { it.name == login && it.server == server }

    override fun editAccount(parentComponent: JComponent, account: GlAccount) {
        TODO("Not yet implemented")
    }
}
