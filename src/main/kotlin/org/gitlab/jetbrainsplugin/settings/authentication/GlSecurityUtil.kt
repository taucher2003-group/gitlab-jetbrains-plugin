/*
Copyright 2000-2022 JetBrains s.r.o.
Licensed under the Apache License, Version 2.0 (the "License").
You may not use this file except in compliance with the License.
You may obtain a copy of the License at www.apache.org/licenses/LICENSE-2.0.txt.
*/
package org.gitlab.jetbrainsplugin.settings.authentication

import com.intellij.openapi.application.ApplicationNamesInfo
import com.intellij.util.Url
import com.intellij.util.Urls.parseEncoded
import org.gitlab.jetbrainsplugin.settings.account.GlServerPath

object GlSecurityUtil {
    private const val API_SCOPE = "api"
    private const val READ_USER_SCOPE = "read_user"
    val MASTER_SCOPES = listOf(API_SCOPE, READ_USER_SCOPE)

    const val DEFAULT_CLIENT_NAME = "GitLab Integration Plugin"

    internal fun buildNewTokenUrl(server: GlServerPath): String {
        require(server.isValid())
        val productName = ApplicationNamesInfo.getInstance().fullProductName

        return server
            .append("/-/profile/personal_access_tokens")!!
            .addParameters(
                mapOf(
                    "name" to "$productName $DEFAULT_CLIENT_NAME",
                    "scopes" to MASTER_SCOPES.joinToString(",")
                )
            )
            .toExternalForm()
    }

    private fun GlServerPath.append(path: String): Url? =
        parseEncoded("${toUrl()}$path")
}
