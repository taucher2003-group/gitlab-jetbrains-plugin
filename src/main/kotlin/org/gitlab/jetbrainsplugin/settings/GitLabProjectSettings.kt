package org.gitlab.jetbrainsplugin.settings

import com.intellij.collaboration.auth.ui.AccountsPanelFactory.accountsPanel
import com.intellij.collaboration.util.ProgressIndicatorsProvider
import com.intellij.ide.DataManager
import com.intellij.openapi.components.service
import com.intellij.openapi.options.BoundConfigurable
import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.DialogPanel
import com.intellij.openapi.util.Disposer
import com.intellij.ui.layout.panel
import org.gitlab.jetbrainsplugin.GitLabBundle.message
import org.gitlab.jetbrainsplugin.settings.account.GlAccountManager
import org.gitlab.jetbrainsplugin.settings.account.GlAccountsDetailsProvider
import org.gitlab.jetbrainsplugin.settings.account.GlAccountsListModel
import org.gitlab.jetbrainsplugin.settings.account.GlDefaultAccountHolder
import vendor.org.jetbrains.plugins.github.authentication.ui.GHAccountsHost

class GitLabProjectSettings(private val project: Project) : BoundConfigurable(message("pluginName")) {
    override fun createPanel(): DialogPanel {
        val defaultAccountHolder = project.service<GlDefaultAccountHolder>()
        val accountsListModel = project.service<GlAccountsListModel>()
        val accountManager = service<GlAccountManager>()
        val indicatorsProvider = ProgressIndicatorsProvider().also {
            Disposer.register(disposable!!, it)
        }
        val detailsProvider = GlAccountsDetailsProvider(indicatorsProvider, accountManager, accountsListModel)
        return panel {
            row {
                accountsPanel(
                    accountManager,
                    defaultAccountHolder,
                    accountsListModel,
                    detailsProvider,
                    disposable!!
                ).also {
                    DataManager.registerDataProvider(it.component) { key ->
                        if (GHAccountsHost.KEY.`is`(key)) accountsListModel
                        else null
                    }
                }
            }
        }
    }
}
