package org.gitlab.jetbrainsplugin

import com.intellij.openapi.util.IconLoader

object GitLabIcons {
    @JvmField
    val Tanuki = IconLoader.getIcon("/icons/tanuki.svg", javaClass)
}
