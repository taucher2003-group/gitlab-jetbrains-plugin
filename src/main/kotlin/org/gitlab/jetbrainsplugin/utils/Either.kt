package org.gitlab.jetbrainsplugin.utils

/**
 * This is a simple implementation of the Either monad, the API is based on [Arrow](https://arrow-kt.io/) so it can be
 * a drop in replacement.
 */
sealed class Either<out A, out B> {
    inline fun <C> fold(processLeft: (A) -> C, processRight: (B) -> C): C = when (this) {
        is Left -> processLeft(value)
        is Right -> processRight(value)
    }

    inline fun <C> map(processRight: (B) -> C): Either<A, C> = when (this) {
        is Left -> this
        is Right -> Right(processRight(this.value))
    }
}

data class Right<out B>(val value: B) : Either<Nothing, B>()

data class Left<out A>(val value: A) : Either<A, Nothing>()

inline fun <A, B> Either<A, B>.getOrHandle(default: (A) -> B): B =
    fold({ default(it) }, { it })
