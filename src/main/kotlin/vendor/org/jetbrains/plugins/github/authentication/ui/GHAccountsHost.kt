/*
Copyright 2000-2022 JetBrains s.r.o.
Licensed under the Apache License, Version 2.0 (the "License").
You may not use this file except in compliance with the License.
You may obtain a copy of the License at www.apache.org/licenses/LICENSE-2.0.txt.
*/
package vendor.org.jetbrains.plugins.github.authentication.ui

import com.intellij.ide.DataManager
import com.intellij.openapi.actionSystem.ActionGroup
import com.intellij.openapi.actionSystem.ActionManager
import com.intellij.openapi.actionSystem.DataKey
import com.intellij.openapi.ui.popup.JBPopupFactory
import com.intellij.openapi.ui.popup.JBPopupFactory.ActionSelectionAid
import com.intellij.ui.components.DropDownLink
import org.gitlab.jetbrainsplugin.settings.account.GlServerPath
import vendor.org.jetbrains.plugins.github.i18n.GithubBundle.message
import javax.swing.JButton

internal interface GHAccountsHost {
    fun addAccount(server: GlServerPath, login: String, token: String) // CHANGE: Github -> Gl
    fun isAccountUnique(login: String, server: GlServerPath): Boolean // CHANGE: Github -> Gl

    companion object {
        val KEY: DataKey<GHAccountsHost> = DataKey.create("GHAccountsHost")

        fun createAddAccountLink(): JButton =
            DropDownLink(message("accounts.add.dropdown.link")) {
                val group = ActionManager.getInstance()
                    .getAction("GiLab.Accounts.AddAccount") as ActionGroup // CHANGE: GitHub -> GitLab
                val dataContext = DataManager.getInstance().getDataContext(it)

                JBPopupFactory.getInstance()
                    .createActionGroupPopup(null, group, dataContext, ActionSelectionAid.MNEMONICS, false)
            }
    }
}
