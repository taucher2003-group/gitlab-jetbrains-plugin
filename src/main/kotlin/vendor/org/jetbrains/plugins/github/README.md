<!-- markdownlint-disable MD044 -->

# vendor.org.jetbrains.plugins.github

This package is originally called `org.jetbrains.plugins.github` and it is copied from
the [JetBrains/intellij-community: IntelliJ IDEA Community Edition & IntelliJ Platform](https://github.com/JetBrains/intellij-community)
project. All source files from this package can be found
in [intellij-community/plugins/github](https://github.com/JetBrains/intellij-community/tree/master/plugins/github).

We had to make some changes to the original files to make them work with the GitLab plugin. All these changes are
annotated with a `// CHANGE` comment. The idea is that eventually, we might extract the common code from GitHub plugin
and reuse it.
