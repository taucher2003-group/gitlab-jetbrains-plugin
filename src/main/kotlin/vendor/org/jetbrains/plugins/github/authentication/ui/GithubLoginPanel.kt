/*
Copyright 2000-2022 JetBrains s.r.o.
Licensed under the Apache License, Version 2.0 (the "License").
You may not use this file except in compliance with the License.
You may obtain a copy of the License at www.apache.org/licenses/LICENSE-2.0.txt.
*/
package vendor.org.jetbrains.plugins.github.authentication.ui

import com.intellij.collaboration.async.CompletableFutureUtil.completionOnEdt
import com.intellij.collaboration.async.CompletableFutureUtil.errorOnEdt
import com.intellij.collaboration.async.CompletableFutureUtil.submitIOTask
import com.intellij.openapi.components.service
import com.intellij.openapi.progress.ProgressIndicator
import com.intellij.openapi.progress.ProgressManager
import com.intellij.openapi.ui.ValidationInfo
import com.intellij.ui.AnimatedIcon
import com.intellij.ui.components.fields.ExtendableTextComponent
import com.intellij.ui.components.fields.ExtendableTextField
import com.intellij.ui.components.panels.Wrapper
import com.intellij.ui.layout.LayoutBuilder
import org.gitlab.jetbrainsplugin.gitlab.api.GlApiRequestExecutor
import org.gitlab.jetbrainsplugin.settings.account.GlServerPath
import vendor.org.jetbrains.plugins.github.i18n.GithubBundle.message
import vendor.org.jetbrains.plugins.github.ui.util.DialogValidationUtils.notBlank
import java.util.concurrent.CompletableFuture
import javax.swing.JComponent
import javax.swing.JTextField

typealias UniqueLoginPredicate = (login: String, server: GlServerPath) -> Boolean // CHANGE Github -> Gl, remove internal

internal class GithubLoginPanel(
    executorFactory: GlApiRequestExecutor.Factory, // CHANGE Github -> Gl
    isAccountUnique: UniqueLoginPredicate
) : Wrapper() {

    private val serverTextField = ExtendableTextField(GlServerPath.DEFAULT_HOST, 0) // CHANGE Github -> Gl
    private var tokenAcquisitionError: ValidationInfo? = null

    private lateinit var currentUi: GHCredentialsUi
    private var tokenUi = GHTokenCredentialsUi(serverTextField, executorFactory, isAccountUnique)
    // CHANGE: Remove OAuth logic since the GitLab plugin won't include it for now
    // private var oauthUi = GHOAuthCredentialsUi(executorFactory, isAccountUnique)

    private val progressIcon = AnimatedIcon.Default()
    private val progressExtension = ExtendableTextComponent.Extension { progressIcon }

    var footer: LayoutBuilder.() -> Unit
        get() = tokenUi.footer
        set(value) {
            tokenUi.footer = value
            // CHANGE: Remove OAuth logic since the GitLab plugin won't include it for now
            // oauthUi.footer = value
            applyUi(currentUi)
        }

    init {
        applyUi(tokenUi)
    }

    private fun applyUi(ui: GHCredentialsUi) {
        currentUi = ui
        setContent(currentUi.getPanel())
        currentUi.getPreferredFocusableComponent()?.requestFocus()
        tokenAcquisitionError = null
    }

    fun getPreferredFocusableComponent(): JComponent? =
        serverTextField.takeIf { it.isEditable && it.text.isBlank() }
            ?: currentUi.getPreferredFocusableComponent()

    fun doValidateAll(): List<ValidationInfo> {
        val uiError =
            notBlank(serverTextField, message("credentials.server.cannot.be.empty"))
                ?: validateServerPath(serverTextField)
                ?: currentUi.getValidator().invoke()

        return listOfNotNull(uiError, tokenAcquisitionError)
    }

    private fun validateServerPath(field: JTextField): ValidationInfo? =
        try {
            GlServerPath.from(field.text) // CHANGE Github -> Gl
            null
        } catch (e: Exception) {
            ValidationInfo(message("credentials.server.path.invalid"), field)
        }

    private fun setBusy(busy: Boolean) {
        serverTextField.apply { if (busy) addExtension(progressExtension) else removeExtension(progressExtension) }
        serverTextField.isEnabled = !busy

        currentUi.setBusy(busy)
    }

    fun acquireLoginAndToken(progressIndicator: ProgressIndicator): CompletableFuture<Pair<String, String>> {
        setBusy(true)
        tokenAcquisitionError = null

        val server = getServer()
        val executor = currentUi.createExecutor()

        return service<ProgressManager>()
            .submitIOTask(progressIndicator) { currentUi.acquireLoginAndToken(server, executor, it) }
            .completionOnEdt(progressIndicator.modalityState) { setBusy(false) }
            .errorOnEdt(progressIndicator.modalityState) { setError(it) }
    }

    fun getServer(): GlServerPath = GlServerPath.from(serverTextField.text.trim())  // CHANGE Github -> Gl

    fun setServer(path: String, editable: Boolean) {
        serverTextField.text = path
        serverTextField.isEditable = editable
    }

    fun setLogin(login: String?, editable: Boolean) {
        tokenUi.setFixedLogin(if (editable) null else login)
    }

    fun setToken(token: String?) = tokenUi.setToken(token.orEmpty())

    fun setError(exception: Throwable?) {
        tokenAcquisitionError = exception?.let { currentUi.handleAcquireError(it) }
    }

    // CHANGE: Remove OAuth logic since the GitLab plugin won't include it for now
    // fun setOAuthUi() = applyUi(oauthUi)
    fun setTokenUi() = applyUi(tokenUi)
}
