/*
Copyright 2000-2022 JetBrains s.r.o.
Licensed under the Apache License, Version 2.0 (the "License").
You may not use this file except in compliance with the License.
You may obtain a copy of the License at www.apache.org/licenses/LICENSE-2.0.txt.
*/
package vendor.org.jetbrains.plugins.github.api.data.request

class GithubRequestPagination @JvmOverloads constructor(
    val pageNumber: Int = 1,
    val pageSize: Int = DEFAULT_PAGE_SIZE
) {
    override fun toString(): String {
        return "page=$pageNumber&per_page=$pageSize"
    }

    companion object {
        /**
         * Max page size
         */
        const val DEFAULT_PAGE_SIZE = 100

        val DEFAULT = GithubRequestPagination()
    }
}
