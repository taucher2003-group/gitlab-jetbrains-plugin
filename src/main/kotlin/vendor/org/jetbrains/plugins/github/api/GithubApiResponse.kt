/*
Copyright 2000-2022 JetBrains s.r.o.
Licensed under the Apache License, Version 2.0 (the "License").
You may not use this file except in compliance with the License.
You may obtain a copy of the License at www.apache.org/licenses/LICENSE-2.0.txt.
*/
package vendor.org.jetbrains.plugins.github.api

import com.intellij.util.ThrowableConvertor
import java.io.IOException
import java.io.InputStream
import java.io.Reader

interface GithubApiResponse {
    fun findHeader(headerName: String): String?

    @Throws(IOException::class)
    fun <T> readBody(converter: ThrowableConvertor<Reader, T, IOException>): T

    @Throws(IOException::class)
    fun <T> handleBody(converter: ThrowableConvertor<InputStream, T, IOException>): T
}
