/*
Copyright 2000-2022 JetBrains s.r.o.
Licensed under the Apache License, Version 2.0 (the "License").
You may not use this file except in compliance with the License.
You may obtain a copy of the License at www.apache.org/licenses/LICENSE-2.0.txt.
*/
package vendor.org.jetbrains.plugins.github.authentication.ui

import com.intellij.openapi.progress.ProgressIndicator
import com.intellij.openapi.ui.ValidationInfo
import com.intellij.ui.layout.LayoutBuilder
import com.intellij.ui.layout.panel
import com.intellij.util.ui.JBEmptyBorder
import com.intellij.util.ui.UIUtil.getRegularPanelInsets
import org.gitlab.jetbrainsplugin.gitlab.api.GlApiRequestExecutor
import org.gitlab.jetbrainsplugin.settings.account.GlServerPath
import vendor.org.jetbrains.plugins.github.ui.util.Validator
import javax.swing.JComponent
import javax.swing.JPanel

internal abstract class GHCredentialsUi {
    abstract fun getPreferredFocusableComponent(): JComponent?
    abstract fun getValidator(): Validator
    abstract fun createExecutor(): GlApiRequestExecutor // CHANGE: Github -> Gl
    abstract fun acquireLoginAndToken(
        server: GlServerPath, // CHANGE: Github -> Gl
        executor: GlApiRequestExecutor, // CHANGE: Github -> Gl
        indicator: ProgressIndicator
    ): Pair<String, String>

    abstract fun handleAcquireError(error: Throwable): ValidationInfo
    abstract fun setBusy(busy: Boolean)

    var footer: LayoutBuilder.() -> Unit = { }

    fun getPanel(): JPanel =
        panel {
            centerPanel()
            footer()
        }.apply {
            // Border is required to have more space - otherwise there could be issues with focus ring.
            // `getRegularPanelInsets()` is used to simplify border calculation for dialogs where this panel is used.
            border = JBEmptyBorder(getRegularPanelInsets())
        }

    protected abstract fun LayoutBuilder.centerPanel()
}
