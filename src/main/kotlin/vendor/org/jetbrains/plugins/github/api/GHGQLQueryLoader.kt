/*
Copyright 2000-2022 JetBrains s.r.o.
Licensed under the Apache License, Version 2.0 (the "License").
You may not use this file except in compliance with the License.
You may obtain a copy of the License at www.apache.org/licenses/LICENSE-2.0.txt.
*/
package vendor.org.jetbrains.plugins.github.api

import com.google.common.annotations.VisibleForTesting
import com.intellij.collaboration.api.graphql.CachingGraphQLQueryLoader
import com.intellij.util.io.isDirectory
import java.nio.file.Files
import java.nio.file.Paths
import java.util.stream.Collectors

object GHGQLQueryLoader : CachingGraphQLQueryLoader() {
    @VisibleForTesting
    fun findAllQueries(): List<String> {
        val url = GHGQLQueryLoader::class.java.classLoader.getResource("graphql/query")!!
        val directory = Paths.get(url.toURI())
        return Files.walk(directory)
            .filter { !it.isDirectory() }
            .map { "graphql/query/" + it.fileName.toString() }
            .collect(Collectors.toList())
    }
}
