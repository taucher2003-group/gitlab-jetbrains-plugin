/*
Copyright 2000-2022 JetBrains s.r.o.
Licensed under the Apache License, Version 2.0 (the "License").
You may not use this file except in compliance with the License.
You may obtain a copy of the License at www.apache.org/licenses/LICENSE-2.0.txt.
*/
package vendor.org.jetbrains.plugins.github.authentication.ui

import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.PlatformCoreDataKeys.CONTEXT_COMPONENT
import com.intellij.openapi.project.DumbAwareAction
import com.intellij.openapi.project.Project
import com.intellij.openapi.util.NlsContexts
import git4idea.i18n.GitBundle
import org.gitlab.jetbrainsplugin.GitLabBundle.message // CHANGE: use GitLab bundle
import org.gitlab.jetbrainsplugin.gitlab.api.GlApiRequestExecutor
import org.gitlab.jetbrainsplugin.settings.account.GlServerPath
import java.awt.Component
import javax.swing.JComponent

class AddGHAccountWithTokenAction : BaseAddAccountWithTokenAction() {
    override val defaultServer: String get() = GlServerPath.DEFAULT_HOST // CHANGE: Github -> Gl
}

class AddGHEAccountAction : BaseAddAccountWithTokenAction() {
    override val defaultServer: String get() = ""
}

abstract class BaseAddAccountWithTokenAction : DumbAwareAction() {
    abstract val defaultServer: String

    override fun update(e: AnActionEvent) {
        e.presentation.isEnabledAndVisible = e.getData(GHAccountsHost.KEY) != null
    }

    override fun actionPerformed(e: AnActionEvent) {
        val accountsHost = e.getData(GHAccountsHost.KEY)!!
        val dialog = newAddAccountDialog(e.project, e.getData(CONTEXT_COMPONENT), accountsHost::isAccountUnique)

        dialog.setServer(defaultServer, defaultServer != GlServerPath.DEFAULT_HOST) // CHANGE: Github -> Gl
        if (dialog.showAndGet()) {
            accountsHost.addAccount(dialog.server, dialog.login, dialog.token)
        }
    }
}

private fun newAddAccountDialog(
    project: Project?,
    parent: Component?,
    isAccountUnique: UniqueLoginPredicate
): BaseLoginDialog =
    GHTokenLoginDialog(project, parent, isAccountUnique).apply {
        title = message("dialog.title.add.github.account")
        setLoginButtonText(message("accounts.add.button"))
    }

internal class GHTokenLoginDialog(project: Project?, parent: Component?, isAccountUnique: UniqueLoginPredicate) :
// CHANGE: Github -> Gl
    BaseLoginDialog(project, parent, GlApiRequestExecutor.Factory.getInstance(), isAccountUnique) {

    init {
        title = message("login.to.github")
        setLoginButtonText(GitBundle.message("login.dialog.button.login"))
        loginPanel.setTokenUi()

        init()
    }

    internal fun setLoginButtonText(@NlsContexts.Button text: String) = setOKButtonText(text)

    override fun createCenterPanel(): JComponent = loginPanel.setPaddingCompensated()
}
