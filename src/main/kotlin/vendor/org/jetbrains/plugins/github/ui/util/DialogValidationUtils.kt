/*
Copyright 2000-2022 JetBrains s.r.o.
Licensed under the Apache License, Version 2.0 (the "License").
You may not use this file except in compliance with the License.
You may obtain a copy of the License at www.apache.org/licenses/LICENSE-2.0.txt.
*/
package vendor.org.jetbrains.plugins.github.ui.util

import com.intellij.openapi.ui.ValidationInfo
import com.intellij.openapi.util.NlsContexts
import javax.swing.JTextField

object DialogValidationUtils {
    /**
     * Returns [ValidationInfo] with [message] if [textField] is blank
     */
    fun notBlank(textField: JTextField, @NlsContexts.DialogMessage message: String): ValidationInfo? {
        return if (textField.text.isNullOrBlank()) ValidationInfo(message, textField) else null
    }

    /**
     * Chains the [validators] so that if one of them returns non-null [ValidationInfo] the rest of them are not checked
     */
    fun chain(vararg validators: Validator): Validator = { validators.asSequence().mapNotNull { it() }.firstOrNull() }

    /**
     * Stateful validator that checks that contents of [textField] are unique among [records]
     */
    class RecordUniqueValidator(
        private val textField: JTextField,
        @NlsContexts.DialogMessage private val message: String
    ) : Validator {
        var records: Set<String> = setOf<String>()

        override fun invoke(): ValidationInfo? =
            if (records.contains(textField.text)) ValidationInfo(message, textField) else null
    }
}

typealias Validator = () -> ValidationInfo?
