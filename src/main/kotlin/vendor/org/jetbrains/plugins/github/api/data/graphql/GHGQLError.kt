/*
Copyright 2000-2022 JetBrains s.r.o.
Licensed under the Apache License, Version 2.0 (the "License").
You may not use this file except in compliance with the License.
You may obtain a copy of the License at www.apache.org/licenses/LICENSE-2.0.txt.
*/
package vendor.org.jetbrains.plugins.github.api.data.graphql

import com.intellij.collaboration.api.dto.GraphQLErrorDTO

/**
 * GitHub returns an additional field [type] here contrary to the spec
 */
class GHGQLError(message: String, val type: String?) : GraphQLErrorDTO(message) {
    override fun toString(): String = message
}
