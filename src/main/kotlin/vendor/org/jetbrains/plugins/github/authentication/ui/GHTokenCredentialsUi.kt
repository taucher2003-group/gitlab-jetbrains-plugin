/*
Copyright 2000-2022 JetBrains s.r.o.
Licensed under the Apache License, Version 2.0 (the "License").
You may not use this file except in compliance with the License.
You may obtain a copy of the License at www.apache.org/licenses/LICENSE-2.0.txt.
*/
package vendor.org.jetbrains.plugins.github.authentication.ui

import com.intellij.ide.BrowserUtil.browse
import com.intellij.openapi.progress.ProgressIndicator
import com.intellij.openapi.ui.ValidationInfo
import com.intellij.ui.DocumentAdapter
import com.intellij.ui.components.JBTextField
import com.intellij.ui.components.fields.ExtendableTextField
import com.intellij.ui.layout.ComponentPredicate
import com.intellij.ui.layout.LayoutBuilder
import org.gitlab.jetbrainsplugin.gitlab.api.GitLabApiRequests
import org.gitlab.jetbrainsplugin.gitlab.api.GlApiRequestExecutor
import org.gitlab.jetbrainsplugin.settings.account.GlServerPath
import org.gitlab.jetbrainsplugin.settings.authentication.GlSecurityUtil
import org.gitlab.jetbrainsplugin.settings.authentication.GlSecurityUtil.buildNewTokenUrl // CHANGE GH -> Gl
import vendor.org.jetbrains.plugins.github.exceptions.GithubAuthenticationException
import vendor.org.jetbrains.plugins.github.exceptions.GithubParseException
import vendor.org.jetbrains.plugins.github.i18n.GithubBundle.message
import vendor.org.jetbrains.plugins.github.ui.util.DialogValidationUtils.notBlank
import vendor.org.jetbrains.plugins.github.ui.util.Validator
import java.net.UnknownHostException
import javax.swing.JComponent
import javax.swing.JTextField
import javax.swing.event.DocumentEvent

internal class GHTokenCredentialsUi(
    private val serverTextField: ExtendableTextField,
    val factory: GlApiRequestExecutor.Factory, // CHANGE Github -> Gl
    val isAccountUnique: UniqueLoginPredicate
) : GHCredentialsUi() {

    private val tokenTextField = JBTextField()
    private var fixedLogin: String? = null

    fun setToken(token: String) {
        tokenTextField.text = token
    }

    override fun LayoutBuilder.centerPanel() {
        row(message("credentials.server.field")) { serverTextField(pushX, growX) }
        row(message("credentials.token.field")) {
            cell {
                tokenTextField(
                    comment = message("login.insufficient.scopes", GlSecurityUtil.MASTER_SCOPES),  // CHANGE GH -> Gl
                    constraints = *arrayOf(pushX, growX)
                )
                button(message("credentials.button.generate")) { browseNewTokenUrl() }
                    .enableIf(serverTextField.serverValid)
            }
        }
    }

    private fun browseNewTokenUrl() = browse(buildNewTokenUrl(serverTextField.tryParseServer()!!))

    override fun getPreferredFocusableComponent(): JComponent = tokenTextField

    override fun getValidator(): Validator = { notBlank(tokenTextField, message("login.token.cannot.be.empty")) }

    override fun createExecutor() = factory.create(tokenTextField.text)

    override fun acquireLoginAndToken(
        server: GlServerPath, // CHANGE Github -> Gl
        executor: GlApiRequestExecutor, // CHANGE Github -> Gl
        indicator: ProgressIndicator
    ): Pair<String, String> {
        val login = acquireLogin(server, executor, indicator, isAccountUnique, fixedLogin)
        return login to tokenTextField.text
    }

    override fun handleAcquireError(error: Throwable): ValidationInfo =
        when (error) {
            is GithubParseException -> ValidationInfo(
                error.message ?: message("credentials.invalid.server.path"),
                serverTextField
            )
            else -> handleError(error)
        }

    override fun setBusy(busy: Boolean) {
        tokenTextField.isEnabled = !busy
    }

    fun setFixedLogin(fixedLogin: String?) {
        this.fixedLogin = fixedLogin
    }

    companion object {
        fun acquireLogin(
            server: GlServerPath,  // CHANGE Github -> Gl
            executor: GlApiRequestExecutor,  // CHANGE Github -> Gl
            indicator: ProgressIndicator,
            isAccountUnique: UniqueLoginPredicate,
            fixedLogin: String?
        ): String {
            // CHANGE remove scope checking, GitLab API doesn't share scopes for token
            val details = executor.execute(
                indicator,
                GitLabApiRequests.CurrentUser.get(server)
            )

            val login = details.username
            if (fixedLogin != null && fixedLogin != login) throw GithubAuthenticationException("Token should match username \"$fixedLogin\"")
            if (!isAccountUnique(login, server)) throw LoginNotUniqueException(login)

            return login
        }

        fun handleError(error: Throwable): ValidationInfo =
            when (error) {
                is LoginNotUniqueException -> ValidationInfo(
                    message(
                        "login.account.already.added",
                        error.login
                    )
                ).withOKEnabled()
                is UnknownHostException -> ValidationInfo(message("server.unreachable")).withOKEnabled()
                is GithubAuthenticationException -> ValidationInfo(
                    message(
                        "credentials.incorrect",
                        error.message.orEmpty()
                    )
                ).withOKEnabled()
                else -> ValidationInfo(
                    message(
                        "credentials.invalid.auth.data",
                        error.message.orEmpty()
                    )
                ).withOKEnabled()
            }
    }
}

private val JTextField.serverValid: ComponentPredicate
    get() = object : ComponentPredicate() {
        override fun invoke(): Boolean = tryParseServer() != null

        override fun addListener(listener: (Boolean) -> Unit) =
            document.addDocumentListener(object : DocumentAdapter() {
                override fun textChanged(e: DocumentEvent) = listener(tryParseServer() != null)
            })
    }

private fun JTextField.tryParseServer(): GlServerPath? = // CHANGE Github -> Gl
    try {
        GlServerPath.from(text.trim()) // CHANGE Github -> Gl
    } catch (e: GithubParseException) {
        null
    }
