/*
Copyright 2000-2022 JetBrains s.r.o.
Licensed under the Apache License, Version 2.0 (the "License").
You may not use this file except in compliance with the License.
You may obtain a copy of the License at www.apache.org/licenses/LICENSE-2.0.txt.
*/
package vendor.org.jetbrains.plugins.github.exceptions;

import org.jetbrains.annotations.Nullable;
import vendor.org.jetbrains.plugins.github.api.data.GithubErrorMessage;

public class GithubStatusCodeException extends GithubConfusingException {
    private final int myStatusCode;
    @Nullable
    private final GithubErrorMessage myError;

    public GithubStatusCodeException(@Nullable String message, int statusCode) {
        this(message, null, statusCode);
    }

    public GithubStatusCodeException(@Nullable String message, @Nullable GithubErrorMessage error, int statusCode) {
        super(message);
        myStatusCode = statusCode;
        myError = error;
    }

    public int getStatusCode() {
        return myStatusCode;
    }

    @Nullable
    public GithubErrorMessage getError() {
        return myError;
    }
}
