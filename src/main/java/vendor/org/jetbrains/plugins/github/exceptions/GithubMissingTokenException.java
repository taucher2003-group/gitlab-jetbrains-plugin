/*
Copyright 2000-2022 JetBrains s.r.o.
Licensed under the Apache License, Version 2.0 (the "License").
You may not use this file except in compliance with the License.
You may obtain a copy of the License at www.apache.org/licenses/LICENSE-2.0.txt.
*/
package vendor.org.jetbrains.plugins.github.exceptions;

import com.intellij.collaboration.auth.ServerAccount;
import org.jetbrains.annotations.NotNull;

public class GithubMissingTokenException extends GithubAuthenticationException {
    public GithubMissingTokenException(@NotNull String message) {
        super(message);
    }

    // CHANGE: In this method, we changed argument to the parent (ServerAccount)
    public GithubMissingTokenException(@NotNull ServerAccount account) {
        this("Missing access token for account " + account);
    }
}
