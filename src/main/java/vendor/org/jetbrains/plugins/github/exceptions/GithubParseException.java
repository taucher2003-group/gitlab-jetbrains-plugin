/*
Copyright 2000-2022 JetBrains s.r.o.
Licensed under the Apache License, Version 2.0 (the "License").
You may not use this file except in compliance with the License.
You may obtain a copy of the License at www.apache.org/licenses/LICENSE-2.0.txt.
*/
package vendor.org.jetbrains.plugins.github.exceptions;

import org.jetbrains.annotations.NotNull;

public class GithubParseException extends RuntimeException {
    public GithubParseException(@NotNull String message) {
        super(message);
    }
}
