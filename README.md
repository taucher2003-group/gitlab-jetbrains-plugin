# GitLab IntelliJ Plugin

<!-- Plugin description -->

This project will contain code for the new GitLab IntelliJ Plugin. See [the initial proof of concept](https://gitlab.com/gitlab-org/gitlab-jetbrains-plugin/-/issues/1#note_814705058) for more details.

<!-- Plugin description end -->
